import { useState, useEffect } from "react";
import axios from 'axios';

export const useFetch = (endpoint, query) => { 
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState (false);
    const [error, setError] = useState (null);

    const options = {
        method: 'GET',
        url: `https://jsearch.p.rapidapi.com/${endpoint}`,
        headers: {
          'X-RapidAPI-Key': '40028bdb09msh6054b703592b158p1fa5a6jsnd55eff778ecb',
          'X-RapidAPI-Host': 'jsearch.p.rapidapi.com'
        },
        params: { ...query }
      };

      const fetchData = async () => {
        setIsLoading(true);

        try{
            const response = await axios.request(options);
            setData(response.data.data);
        } catch (error) {
            setError(error);
            alert(error)
        } finally {
            setIsLoading(false);
        }
      }

      useEffect(()=>{
        fetchData();
      }, [])

      const refetch = () => {
        setIsLoading(true);
        fetchData();
      }

      return {
        data,
        isLoading,
        error,
        refetch
      }
}
